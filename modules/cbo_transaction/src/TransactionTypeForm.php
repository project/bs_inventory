<?php

namespace Drupal\cbo_transaction;

use Drupal\cbo_transaction\Entity\TransactionType;
use Drupal\Core\Entity\BundleEntityFormBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form handler for transaction type forms.
 */
class TransactionTypeForm extends BundleEntityFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state) {
    $form = parent::form($form, $form_state);

    $type = $this->entity;
    if ($this->operation == 'add') {
      $form['#title'] = $this->t('Add transaction type');
    }
    else {
      $form['#title'] = $this->t('Edit %label transaction type', ['%label' => $type->label()]);
    }

    $form['label'] = [
      '#title' => t('Name'),
      '#type' => 'textfield',
      '#default_value' => $type->label(),
      '#required' => TRUE,
      '#size' => 30,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $type->id(),
      '#maxlength' => EntityTypeInterface::BUNDLE_MAX_LENGTH,
      '#machine_name' => [
        'exists' => ['Drupal\cbo_transaction\Entity\TransactionType', 'load'],
        'source' => ['label'],
      ],
      '#description' => t('A unique machine-readable name for this transaction type. It must only contain lowercase letters, numbers, and underscores. This name will be used for constructing the URL of the %transaction-add page, in which underscores will be converted into hyphens.', [
        '%transaction-add' => t('Add transaction'),
      ]),
    ];

    $form['description'] = [
      '#title' => t('Description'),
      '#type' => 'textfield',
      '#default_value' => $type->getDescription(),
      '#size' => 30,
    ];

    $entities = $this->entityTypeManager->getStorage('transaction_source_type')->loadMultiple();
    $options = array_map(function ($entity) {
      return $entity->label();
    }, $entities);
    $form['source_type'] = [
      '#title' => t('Source Type'),
      '#type' => 'select',
      '#options' => $options,
      '#default_value' => $type->getSourceType(),
    ];

    $form['action'] = [
      '#title' => t('Action'),
      '#type' => 'select',
      '#options' => TransactionType::$transaction_actions,
      '#default_value' => $type->getAction(),
    ];

    return $this->protectBundleIdElement($form);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);

    $id = trim($form_state->getValue('id'));
    // '0' is invalid, since elsewhere we check it using empty().
    if ($id == '0') {
      $form_state->setErrorByName('id', $this->t("Invalid machine-readable name. Enter a name other than %invalid.", ['%invalid' => $id]));
    }
  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {
    $type = $this->entity;
    $type->set('id', trim($type->id()));
    $type->set('label', trim($type->label()));

    $status = $type->save();

    $t_args = ['%name' => $type->label()];

    if ($status == SAVED_UPDATED) {
      drupal_set_message(t('The transaction type %name has been updated.', $t_args));
    }
    elseif ($status == SAVED_NEW) {
      drupal_set_message(t('The transaction type %name has been added.', $t_args));
      $context = array_merge($t_args, ['link' => $type->link($this->t('View'), 'collection')]);
      $this->logger('transaction')->notice('Added transaction type %name.', $context);
    }
  }

}
