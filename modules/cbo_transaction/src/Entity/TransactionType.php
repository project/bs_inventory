<?php

namespace Drupal\cbo_transaction\Entity;

use Drupal\Core\Config\Entity\ConfigEntityBundleBase;
use Drupal\cbo_transaction\TransactionTypeInterface;
use Drupal\Core\Entity\EntityStorageInterface;

/**
 * Defines the Transaction Type configuration entity.
 *
 * @ConfigEntityType(
 *   id = "transaction_type",
 *   label = @Translation("Transaction Type"),
 *   handlers = {
 *     "access" = "Drupal\Core\Entity\EntityAccessControlHandler",
 *     "form" = {
 *       "default" = "Drupal\cbo_transaction\TransactionTypeForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *     "list_builder" = "Drupal\cbo_transaction\TransactionTypeListBuilder",
 *   },
 *   admin_permission = "administer transaction types",
 *   config_prefix = "type",
 *   bundle_of = "transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label"
 *   },
 *   links = {
 *     "add-form" = "/admin/transaction/type/add",
 *     "edit-form" = "/admin/transaction/type/{transaction_type}/edit",
 *     "delete-form" = "/admin/transaction/type/{transaction_type}/delete",
 *     "collection" = "/admin/transaction/type",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "source_type",
 *     "action",
 *   }
 * )
 */
class TransactionType extends ConfigEntityBundleBase implements TransactionTypeInterface {

  public static $transaction_actions = [
    'issue_from_store' => 'Issue from stores',
    'subinventory_transfer' => 'Subinventory transfer',
    'direct_organization_transfer' => 'Direct organization transfer',
    'cycle_count_adjustment' => 'Cycle count adjustment',
    'physical_inventory_adjustment' => 'Physical inventory adjustment',
    'intransit_receipt' => 'Intransit receipt',
    'intransit_shipment' => 'Intransit shipment',
    'cost_update' => 'Cost update',
    'receipt_into_stores' => 'Receipt into stores',
    'delivery_adjustments' => 'Delivery adjustments',
    'wip_assembly_scrap' => 'WIP assembly scrap',
    'assembly_completion' => 'Assembly completion',
    'assembly_return' => 'Assembly return',
    'negative_component_issue' => 'Negative component issue',
    'negative_component_return' => 'Negative component return',
    'staging_transfer' => 'Staging Rransfer',
    'ownership_transfer' => 'Ownership Transfer',
    'logical_issue' => 'Logical Issue',
    'logical_delivery_adjustment' => 'Logical Delivery Adjustment',
    'retroactive_price_adjustment' => 'Retroactive Price Adjustment',
    'logical_receipt' => 'Logical Receipt',
    'delivery_adjustment' => 'Delivery Adjustment',
    'lot_split' => 'Lot Split',
    'lot_merge' => 'Lot Merge',
    'lot_translate' => 'Lot Translate',
    'lot_update_quantity' => 'Lot Update Quantity',
    'logical_expense_requisition_receipt' => 'Logical Expense Requisition Receipt',
    'planning_transfer' => 'Planning Transfer',
    'ownership_transfer' => 'Ownership Transfer',
    'logical_intercompany_sales' => 'Logical Intercompany Sales',
    'logical_intercompany_receipt' => 'Logical Intercompany Receipt',
    'logical_intercompany_receipt_return' => 'Logical Intercompany Receipt Return',
    'logical_intercompany_sales_return' => 'Logical Intercompany Sales Return',
    'container_pack' => 'Container Pack',
    'container_unpack' => 'Container Unpack',
    'container_split' => 'Container Split',
    'cost_group_transfer' => 'Cost Group Transfer',
  ];

  /**
   * The machine name of this Transaction Type.
   *
   * @var string
   */
  protected $id;

  /**
   * The human-readable name of the Transaction Type.
   *
   * @var string
   */
  protected $label;

  /**
   * The transaction source type.
   *
   * @var string
   */
  protected $source_type;

  /**
   * The transaction action.
   *
   * @var string
   */
  protected $action;

  /**
   * A brief description of this Transaction Type.
   *
   * @var string
   */
  protected $description;

  /**
   * {@inheritdoc}
   */
  public function postSave(EntityStorageInterface $storage, $update = TRUE) {
    parent::postSave($storage, $update);

    if (!$update) {
      if ($this->action == 'direct_organization_transfer') {
        cbo_transaction_add_to_organization_field($this);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->description;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription($description) {
    $this->description = $description;
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSourceType() {
    return $this->source_type;
  }

  /**
   * {@inheritdoc}
   */
  public function getAction() {
    return $this->action;
  }

}
